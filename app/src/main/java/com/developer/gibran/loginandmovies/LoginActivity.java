package com.developer.gibran.loginandmovies;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.gibran.beans.User;
import com.developer.gibran.controllers.UserController;


public class LoginActivity extends ActionBarActivity {

    private EditText etUser, etPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        UserController.addUser(new User("Gibran", "Polonsky", 58198793, "JoseGibran", "123"));
        UserController.addUser(new User("Jorge", "Monterroso", 4564654, "JorgeMonte", "123"));
        etUser = (EditText)findViewById(R.id.etUser);
        etPassword = (EditText)findViewById(R.id.etPassword);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick_btnLogin(View view) {
        User logedUser = UserController.Logging(etUser.getText().toString(), etPassword.getText().toString());
        if(logedUser!=null){
            Toast.makeText(this, "Bienvenido: " + logedUser.getFirstName(), Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(this, MainActivity.class);

            Bundle extras = intent.getExtras();
            Bundle mBundle = new Bundle();
            mBundle.putString("userName", logedUser.getUserName());
            intent.putExtras(mBundle);
            startActivity(intent);
        }else{
            Toast.makeText(this, "Usuario o Password incorrecto", Toast.LENGTH_SHORT).show();
        }
    }
    public void onClick_btnRegister(View view){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

}
