package com.developer.gibran.loginandmovies;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.developer.gibran.beans.User;
import com.developer.gibran.controllers.UserController;

public class RegisterActivity extends ActionBarActivity {

    private EditText etName, etLastName, etPhone, etUserName, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        etName = (EditText)findViewById(R.id.etName);
        etLastName = (EditText)findViewById(R.id.etLastName);
        etPhone = (EditText)findViewById(R.id.etPhone);
        etUserName = (EditText)findViewById(R.id.etUserName);
        etPassword = (EditText)findViewById(R.id.etPassword);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void onClick_btnRegister(View view){
        User newUser = new User(etName.getText().toString(), etLastName.getText().toString(),
                Integer.parseInt(etPhone.getText().toString()), etUserName.getText().toString(), etPassword.getText().toString());
        UserController.addUser(newUser);
        Toast.makeText(this, "Bienvenido: " + newUser.getFirstName(), Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, MainActivity.class);

        Bundle extras = intent.getExtras();
        Bundle mBundle = new Bundle();
        mBundle.putString("userName", newUser.getUserName());
        intent.putExtras(mBundle);
        startActivity(intent);
    }
}
