package com.developer.gibran.loginandmovies;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.developer.gibran.beans.User;
import com.developer.gibran.controllers.UserController;
import com.developer.gibran.beans.Movie;

import java.util.ArrayList;

public class MainActivity extends ActionBarActivity {
    private User currentUser;

    private Resources res ;
    private TabHost tabs ;
    private ArrayList<Movie> movies = new ArrayList<Movie>();
    private ArrayList<Movie> favoritos = new ArrayList<Movie>();

    private ListView lvPeliculas, lvFavoritos;

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment)getSupportFragmentManager().findFragmentById(R.id.navigation_drawer_fragment);

        drawerFragment.setUp((DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);


        try{
            currentUser = UserController.getUserByUserName(getIntent().getExtras().getString("userName"));
        }catch(Exception e){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            e.printStackTrace();
        }
        res = getResources();
        tabs=(TabHost)findViewById(android.R.id.tabhost);
        tabs.setup();
        TabHost.TabSpec spec = tabs.newTabSpec("Peliculas");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Peliculas", res.getDrawable(android.R.drawable.ic_btn_speak_now));
        tabs.addTab(spec);

        spec = tabs.newTabSpec("Favoritos");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Favoritos", res.getDrawable(android.R.drawable.ic_dialog_map));
        tabs.addTab(spec);
        tabs.setCurrentTab(0);


        movies.add(new Movie("Avengers", "Accion", "sdfsd"));
        movies.add(new Movie("GYJOE", "Accion", "sdfsd"));
        movies.add(new Movie("Harry Potter", "Accion", "sdfsd"));
        movies.add(new Movie("Divergente", "Accion", "sdfsd"));
        movies.add(new Movie("Los Juegos del Hambre", "Accion", "sdfsd"));
        movies.add(new Movie("Jerlock Holmes", "Accion", "sdfsd"));
        movies.add(new Movie("Ted 2 ", "Accion", "sdfsd"));
        movies.add(new Movie("High School of the dead", "Accion", "sdfsd"));
        movies.add(new Movie("Pockemon", "Accion", "sdfsd"));
        movies.add(new Movie("Dragon Ball Z", "Accion", "sdfsd"));



        lvPeliculas = (ListView)findViewById(R.id.lvPeliculas);
        lvFavoritos = (ListView)findViewById(R.id.lvFavoritos);

       // ArrayAdapter<String> adapterMovies = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, movies);
        AdaptadorMovies adapterMovies = new AdaptadorMovies(this,  movies);
        lvPeliculas.setAdapter(adapterMovies);

        registerForContextMenu(lvPeliculas);



    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();

        if(v.getId()== R.id.lvPeliculas){
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            //String peliculaString = lvPeliculas.getAdapter().getItem(info.position).toString();
            Movie stringMovie = (Movie)lvPeliculas.getAdapter().getItem(info.position);

            menu.setHeaderTitle( stringMovie.getTitle());
            inflater.inflate(R.menu.menu_ctx_lista_pelicula, menu);
        }
    }
    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        switch(item.getItemId()){
            case R.id.ctxAgregarFavorito:
                Toast.makeText(this, "Pelicula Agregada a Favoritos", Toast.LENGTH_SHORT).show();
                favoritos.add(movies.get(info.position));
                AdaptadorMovies adapterFavorites = new AdaptadorMovies(this,  favoritos);
                lvFavoritos.setAdapter(adapterFavorites);

                return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class AdaptadorMovies extends ArrayAdapter<Movie>{
        public AdaptadorMovies(Context context, ArrayList<Movie> datos){
            super(context, R.layout.listitem_movie, datos);
        }

        public View getView(int position, View convertView, ViewGroup parent){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View item = inflater.inflate(R.layout.listitem_movie, null);

            TextView tvTitle = (TextView)item.findViewById(R.id.tvTitle);
            TextView tvGenre = (TextView)item.findViewById(R.id.tvGenre);
            TextView tvAuthor = (TextView)item.findViewById(R.id.tvAuthor);

            String titleMovie = movies.get(position).getTitle();
            String genreMovies = movies.get(position).getGenre();
            String authorMovies = movies.get(position).getAuthor();

            tvTitle.setText(titleMovie);
            tvGenre.setText(genreMovies);
            tvAuthor.setText(authorMovies);

            return item;
        }
    }


}
