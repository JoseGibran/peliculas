package com.developer.gibran.controllers;

/**
 * Created by Gibran on 18/05/2015.
 */
import java.util.ArrayList;
import com.developer.gibran.beans.User;
public class UserController {
    private static ArrayList<User> userList = new ArrayList<User>() ;


    public static ArrayList<User> getUserList(){
        return userList;
    }

    public static User Logging(String username, String password){
        for(User user: getUserList()){
            if(user.getUserName().equals(username)){
                if(user.getPassword().equals(password)){
                    return user;
                }
                return null;
            }
        }
        return null;
    }
    public static User getUserByUserName(String username){
        for(User user: getUserList()){
            if(user.getUserName().equals(username)){
                return user;
            }
        }
        return null;
    }
    public static boolean unique(String userName){
        for(User user : getUserList()){
            if(user.getUserName().equalsIgnoreCase(userName)){
                return false;
            }
        }
        return true;
    }

    public static boolean addUser(User user){
        if(unique(user.getUserName())){
            getUserList().add(user);
            return true;
        }else{
            return false;
        }

    }
    public static void removeUser(User user){
        getUserList().remove(user);
    }
}
